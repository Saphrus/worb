﻿using UnityEngine;
using System.Collections;

public class FizzBuzz : MonoBehaviour {

	public int foobar = 0;
	
	void Start () {
		while (foobar < 100) {
			foobar++;
			if (foobar % 3 == 0 && foobar % 5 != 0)
				Debug.Log ("Fizz");
			else if (foobar % 5 == 0 && foobar % 3 != 0)
				Debug.Log ("Buzz");
			else if (foobar % 5 == 0 && foobar % 3 == 0)
				Debug.Log ("FizzBuzz");
			else if (foobar % 5 != 0 && foobar % 3 != 0)
				Debug.Log (foobar + "");
		}
	}
}
