﻿using UnityEngine;
using System.Collections;

public class ViewMode : MonoBehaviour {

	public GameObject OculusCamera;
	public GameObject NormalCamera;
	public int CameraMode = 1;

	void Awake() {
		if (networkView.isMine){
			if (CameraMode == 0) {
				OculusCamera.SetActive (true);
				NormalCamera.SetActive (false);
			}
			
			if (CameraMode == 1) {
				OculusCamera.SetActive (false);
				NormalCamera.SetActive (true);
			}
			
		}else{
			OculusCamera.SetActive (false);
			NormalCamera.SetActive (false);
		}
	}
}