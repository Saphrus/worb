﻿using UnityEngine;
using System.Collections;

public class MusicIn : MonoBehaviour {
	
	public AudioSource AudioSource1;
	public AudioClip Track1;
	public float Track1Volume = 0.0f;
	public float Speed = 0.1f;
	public float MaxVolume = 0.5f;

	void Update () {
		if(Track1Volume < MaxVolume){
			Track1Volume += Speed * Time.deltaTime;
			AudioSource1.audio.volume = Track1Volume;
		}
	}
}