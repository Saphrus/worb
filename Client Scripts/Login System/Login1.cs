﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class Login1 : MonoBehaviour {
	public InternetCalls calls;
	public string User;
	public string Password;
	public string RegUser;
	public string RegPassword;
	public string RegEmail;
	public string RegConfirmPassword;
	public GUIStyle GUINormal;
	public GUIStyle GUITitles;
	public GUIStyle GUIErrors;
	public string IPAddress;
    private bool IsValidEmail(string strIn){return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");}

	private void OnGUI(){
		GUILayout.BeginArea (new Rect ((Screen.width / 2) - 350, (Screen.height / 2) - 40, 256, 24), GUIContent.none, "box");
		GUILayout.Label ("Login", GUITitles);
		GUILayout.EndArea ();

		// Login

		GUILayout.BeginArea (new Rect ((Screen.width / 2) - 414, (Screen.height / 2) - 10, 384, 256), GUIContent.none, "box");
		GUILayout.BeginVertical();
		GUILayout.Label ("Username", GUINormal);
		User = GUILayout.TextField (User);
		GUILayout.EndVertical ();
		GUILayout.BeginVertical();
		GUILayout.Label ("Password", GUINormal);
		Password = GUILayout.PasswordField (Password, '*', 24);
		GUILayout.EndVertical ();
		if (GUILayout.Button ("Login")){
			calls.DoLogin(User, Password);
		}
		GUILayout.Label (InternetCalls.loginStatus, GUIErrors);	
		GUILayout.EndArea ();

		// 

		GUILayout.BeginArea (new Rect ((Screen.width / 2) + 94, (Screen.height / 2) - 40, 256, 24), GUIContent.none, "box");
		GUILayout.Label ("Register", GUITitles);
		GUILayout.EndArea ();

		// Register
		
		GUILayout.BeginArea (new Rect ((Screen.width / 2) + 30, (Screen.height / 2) - 10, 384, 256), GUIContent.none, "box");
		GUILayout.BeginVertical();
		GUILayout.Label ("Username", GUINormal);
		RegUser = GUILayout.TextField (RegUser);
		GUILayout.EndVertical ();
		GUILayout.BeginVertical();
		GUILayout.Label ("Email", GUINormal);
		RegEmail = GUILayout.TextField (RegEmail);
		GUILayout.EndVertical ();
		GUILayout.BeginVertical();
		GUILayout.Label ("Password", GUINormal);
		RegPassword = GUILayout.PasswordField (RegPassword, "*"[0], 24);
		GUILayout.EndVertical ();
		GUILayout.BeginVertical();
		GUILayout.Label ("Confirm Password", GUINormal);
		RegConfirmPassword = GUILayout.PasswordField (RegConfirmPassword, "*"[0], 24);
		GUILayout.EndVertical ();
		if (RegUser.Length > 3){
			if (IsValidEmail(RegEmail)){
				if (RegPassword.Length < 6) {
					GUILayout.Button ("Register");
					GUILayout.Label ("Password must be 6-24 characters long.", GUIErrors);
				}else{
					if (RegPassword == RegConfirmPassword) {
						if (GUILayout.Button ("Register")) {
							calls.DoRegister (RegUser, RegPassword, RegEmail);
							}
				}else{
					GUILayout.Button ("Register");
					GUILayout.Label ("Passwords don't match.", GUIErrors);
					}
				}
			}else{
			GUILayout.Button ("Register");
			GUILayout.Label ("Please enter a valid email.", GUIErrors);
			}
		}else{
			GUILayout.Button ("Register");
			GUILayout.Label ("Username must be 4-24 characters long.", GUIErrors);
		}
		GUILayout.Label (InternetCalls.registrationStatus, GUIErrors);
		GUILayout.EndArea ();
	}
}