﻿using UnityEngine;
using System.Collections;

public class WallJump : MonoBehaviour {

	public string wallTag = "Wall Pad";

	void OnCollisionEnter (Collision col) {
		if(col.gameObject.tag == wallTag){
			// Do something
		}

		else if (col.gameObject.tag != wallTag){
			// Do Nothing
		}
	}
}
