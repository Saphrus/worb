﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour {
	public float XGravity = 0f;
	public float YGravity = -1f;
	public float ZGravity = 0f;
	private bool DestroyOnLoad = true;

	void Start () {
		Physics.gravity = new Vector3(ZGravity, YGravity, XGravity);

		if (DestroyOnLoad)
		Component.Destroy (this);
	}
}
