using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 10f;
	public float friction = 10f;
	public float directionSpeedX = 0f;
	public float directionSpeedZ = 0f;
	private float verticalInput = Input.GetAxis("Vertical");

	void Update()
	{
		if (networkView.isMine)
		{
			InputMovement();
		}
	}
	
	void InputMovement()
	{

		// Movement Controls

		if (Input.GetAxis ("Vertical") != 0){
			rigidbody.MovePosition (rigidbody.position + transform.forward * verticalInput * Time.deltaTime);
			directionSpeedZ = 50f;
		}

		if (Input.GetAxis ("Horizontal") != 0) {
			rigidbody.MovePosition (rigidbody.position + transform.right * verticalInput * Time.deltaTime);
			directionSpeedX = 50f;
		}

		// Direction Speed Variables

		if (directionSpeedZ < 1)
			directionSpeedZ += friction * Time.deltaTime;

		if (directionSpeedZ > -1)
			directionSpeedZ -= friction * Time.deltaTime;

		if (directionSpeedX < 1)
			directionSpeedX += friction * Time.deltaTime;

		if (directionSpeedX > -1)
			directionSpeedX -= friction * Time.deltaTime;

	}
}