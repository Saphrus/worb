﻿using UnityEngine;
using System.Collections;

public class ControlSettings : MonoBehaviour {

	public GUISkin guiSkin;
	public Color menuColor = new Color(0.09f, 0.77f, 1, 1);

	void Start() {

		// Initialize cInput
		cInput.Init();

		// cGUI SETUP
		cGUI.cSkin = guiSkin;
		cGUI.bgColor = menuColor;

		// Maximum Size of Window
		cGUI.windowMaxSize = new Vector2(Screen.width - 150, Screen.height - 100);

		// Modifier Keys
		cInput.AddModifier(KeyCode.LeftAlt);
		cInput.AddModifier(KeyCode.RightAlt);
		
		// Default Keybinds
		cInput.SetKey("Forward", Keys.W, Keys.UpArrow);
		cInput.SetKey("Backward", Keys.S, Keys.DownArrow);
		cInput.SetKey("Left", Keys.A, Keys.LeftArrow); 
		cInput.SetKey("Right", Keys.D, Keys.RightArrow);
		cInput.SetKey("Boost", Keys.LeftShift, Keys.RightShift);

		cInput.SetKey("Fire", Keys.Mouse0);
		cInput.SetKey("Alternate Fire", Keys.Mouse1);

		cInput.SetKey("Weapon 1", Keys.Alpha1);
		cInput.SetKey("Weapon 2", Keys.Alpha2);
		cInput.SetKey("Weapon 3", Keys.Alpha3);
		cInput.SetKey("Weapon 4", Keys.Alpha4);
		cInput.SetKey("Weapon 5", Keys.Alpha5);

		cInput.SetAxis("Horizontal", "Left", "Right");
		cInput.SetAxis("Vertical", "Forward", "Backward");

	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape) && !cInput.scanning) {
			cGUI.ToggleGUI();
		}
	}

}
