﻿using UnityEngine;
using System.Collections;

public class MouseLookX : MonoBehaviour {

	public float Sensitivity = 540f;

	void Update()
	{
		if (networkView.isMine)
		{
			MouseLook();
		}
	}

	void MouseLook()
	{
		if (Input.GetAxis("Mouse X") != 0)
			transform.Rotate(0, Input.GetAxis("Mouse X") * Time.deltaTime * Sensitivity, 0);
	}
}
