﻿using UnityEngine;
using System.Collections;

public class MouseLookY : MonoBehaviour {

	public float Sensitivity = -540f;

	void Update()
	{
		if (networkView.isMine)
		{
			MouseLook();
		}
	}

	void MouseLook()
	{
		if (Input.GetAxis("Mouse Y") != 0)
			transform.Rotate(Input.GetAxis("Mouse Y") * Time.deltaTime * Sensitivity, 0, 0);
	}
}
