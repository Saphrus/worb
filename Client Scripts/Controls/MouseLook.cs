﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {
	
	public float Sensitivity = -540f;
	public bool XEnabled;
	public bool YEnabled;
	public bool YInverted;

	void Start() {
		if (YInverted)
			Sensitivity = -Sensitivity;
	}
	
	void Update(){
		if (networkView.isMine){
			if (XEnabled)
				MouseLookX();
			if (YEnabled)
				MouseLookY();
		}
	}
	
	void MouseLookX() {
		if (Input.GetAxis("Mouse X") != 0)
			transform.Rotate(0, Input.GetAxis("Mouse X") * Time.deltaTime * Sensitivity, 0);
	}

	void MouseLookY() {
		if (Input.GetAxis("Mouse Y") != 0)
			transform.Rotate(Input.GetAxis("Mouse Y") * Time.deltaTime * Sensitivity, 0, 0);
	}
}
